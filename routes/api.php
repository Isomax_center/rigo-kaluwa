<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'API\AuthController@login');
Route::post('register', 'API\AuthController@register');

Route::middleware('jwt.auth')->group(function(){
    
    Route::get('logout', 'API\AuthController@logout');

    Route::get('dependentes', 'DependenteController@index');

    Route::get('dependentes/{dependente}', 'DependenteController@show');
    Route::post('dependentes', 'DependenteController@store');
    Route::put('dependentes/{dependente}', 'DependenteController@update');
    Route::delete('dependentes/{dependente}', 'DependenteController@delete');


    Route::get('clientes', 'ClienteController@index');
    Route::get('clientes/{cliente}', 'ClienteController@show');
    Route::post('clientes', 'ClienteController@store');
    Route::put('clientes/{Cliente}', 'ClienteController@update');
    Route::delete('clientes/{cliente}', 'ClienteController@delete');

});




// API Route

// Route::prefix('v1')->namespace('API')->group(function () {

//     Route::get('login', function(Request $request) {
//       return response()->json(['error'=>'Unauthorized Access']);
//     });
  
//     Route::post('/login','AuthController@postLogin');
//     Route::post('/register','AuthController@postRegister');
  
//     Route::middleware('APIToken')->group(function () {
//       Route::post('/logout','AuthController@postLogout');
//     });
  
//   });
  
//   