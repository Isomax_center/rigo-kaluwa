<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Cliente extends Model
{
    protected $fillable = [
        'nome', 'email', 'telefone','user_id'
    ];


    public function dependente()
    {
        return $this->BelongsTo('App\Models\Depedente');
    }

    public function user()
    {
        return $this->BelongsTo('App\User');
    }

    public function rules()
    {
        return [
            'nome' => 'required',
            'email' => 'required',
            'telefone' => 'required',
            'user_id' => 'required',
        ];
    }

}
