<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Dependente extends Model
{
    protected $fillable = [
        'nome', 'email', 'celular','cliente_id','user_id'
    ];


    public function clientes()
    {
        return $this->hasMany('App\Models\Cliente');
    }

    public function usres()
    {
        return $this->hasMany('App\User');
    }

    public function rules()
    {
        return [
            'nome' => 'required',
            'email' => 'required',
            'celular' => 'required',
            'cliente_id' => 'required',
            'user_id' => 'required',
        ];
    }

}
