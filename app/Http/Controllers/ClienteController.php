<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;
use App\Models\Cliente;
use App\Models\Dependente;
use App\User;
use Illuminate\Support\Facades\Storage;

class ClienteController extends Controller
{
    
    public function index()
    {
        return Cliente::all();
    }

    public function show(Cliente $cliente)
    {
        return $cliente;
    }

    public function store(Request $request)
    {
        
        $cliente = Cliente::create([
            'nome' => $request->nome,
            'email' => $request->email,
            'telefone' => $request->telefone,
            'user_id' => $request->user()->id,
           
          ]);
    
          return response()->json($cliente,  201);
    }

    public function update(Request $request, Cliente $cliente)
    {
            // check if currently authenticated user is the owner 
      if ($request->user()->id !== $cliente->user_id) {
        return response()->json(['error' => 'You can only edit your own stuff.'], 403);
      }

      $cliente->update($request->only(['nome', 'email','telefone','user_id']));

      return response()->json($cliente, 200);
    
    }

    public function delete(Request $request,Cliente $cliente)
    {
              // check if currently authenticated user is the owner 
      if ($request->user()->id !== $cliente->user_id) {
        return response()->json(['error' => 'You can only delete your own stuff.'], 403);
      }

      $cliente->delete();
      return response()->json(null, 204);
       
    }
}
