<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;
use App\Models\Dependente;
use App\Models\Cliente;
use App\User;
use Sentinel;
use Session;

use Illuminate\Support\Facades\Storage;

class DependenteController extends Controller
{
    public function index()
    {
       
        return Dependente::all();
    }

    public function show(Dependente $dependente)
    {
        return $dependente;
    }

    public function store(Request $request)
    {
       
        $dependente = Dependente::create($request->all());
        return response()->json($dependente,  201);
    }

    public function update(Request $request, Dependente $dependente)
    {
        $id = Auth::id();
        $ownerId = Dependente::select('select user_id from nodejs.dependentes  where id=?');
       
        if($id == $ownerId){
            $dependente->update($request->all());

            return response()->json($dependente, 200);
        }else {
            return "You are not allowed to do this operation";
        }
    }

    public function delete(Dependente $dependente)
    {
        $id = Auth::id();
        $ownerId = DB::select('select user_id from nodejs.dependentes  where id=?');
       
        if($id == $ownerId){
            $dependente->delete();

            return response()->json(null, 204);
        }else {
            return "You are not allowed to do this operation";
        }
       
    }
}
