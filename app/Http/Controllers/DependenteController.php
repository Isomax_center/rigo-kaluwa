<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;
use App\Models\Dependente;
use App\Models\Cliente;
use App\User;
use Sentinel;
use Session;

use Illuminate\Support\Facades\Storage;

class DependenteController extends Controller
{
    public function index()
    {
       
        return Dependente::all();
    }

    public function show(Dependente $dependente)
    {
        return $dependente;
    }

    public function store(Request $request,cliente $clientes)
    {
        $clientes = Cliente::get()->all(['id', 'nome']);
        $dependente = Dependente::create([
            'nome' => $request->nome,
            'email' => $request->email,
            'celular' => $request->celular,
            'cliente_id' => $request->cliente_id,
            'user_id' => $request->user()->id,
           
          ]);
    
          return response()->json($dependente,  201);

       
    }

    public function update(Request $request, Dependente $dependente)
    {
         // check if currently authenticated user is the owner 
      if ($request->user()->id !== $dependente->user_id) {
        return response()->json(['error' => 'You can only edit your own stuff.'], 403);
      }

      $dependente->update($request->only(['nome', 'email','celular','cliente_id']));

      return response()->json($dependente, 200);

      
    }

    public function delete(Request $request,Dependente $dependente)
    {

           // check if currently authenticated user is the owner 
      if ($request->user()->id !== $dependente->user_id) {
        return response()->json(['error' => 'You can only delete your own stuff.'], 403);
      }

      $dependente->delete();
      return response()->json(null, 204);
       
    }
}
